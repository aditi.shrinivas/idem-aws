import tempfile
from unittest import mock

import pytest
import yaml


@pytest.mark.asyncio
async def test_gather(hub):
    ACCT = """
    aws:
      default:
        aws_access_key_id: my_key_id
        aws_secret_access_key: my_secret_key
        # region_name
    """

    PROFILES = dict(profiles=yaml.safe_load(ACCT))

    CONFIG = b"""
    acct:
      extras:
        aws:
          region_name: mock-region
    """

    with tempfile.NamedTemporaryFile(suffix=".cfg", delete=True) as fh:
        fh.write(CONFIG)
        fh.flush()

        with mock.patch("sys.argv", ["idem", "state", f"--config={fh.name}"]):
            hub.pop.config.load(
                ["idem", "acct", "rend", "evbus"], "idem", parse_cli=True
            )

    assert hub.OPT.acct.extras == {"aws": {"region_name": "mock-region"}}

    ctx = await hub.idem.acct.ctx(
        path="states.aws.", acct_profile="default", acct_data=PROFILES
    )

    assert ctx.acct.get("region_name") == "mock-region", hub.OPT.acct.extras


@pytest.mark.asyncio
async def test_gather_esm_extras(hub):
    """
    Verify that esm region name can be specified
    """
    ACCT = """
    aws:
      default:
        aws_access_key_id: my_key_id
        aws_secret_access_key: my_secret_key
        esm:
          foo: bar
    """

    PROFILES = dict(profiles=yaml.safe_load(ACCT))

    CONFIG = b"""
    acct:
      extras:
        aws:
          esm:
            region_name: mock-region
    """

    with tempfile.NamedTemporaryFile(suffix=".cfg", delete=True) as fh:
        fh.write(CONFIG)
        fh.flush()

        with mock.patch("sys.argv", ["idem", "state", f"--config={fh.name}"]):
            hub.pop.config.load(
                ["idem", "acct", "rend", "evbus"], "idem", parse_cli=True
            )

    assert hub.OPT.acct.extras == {"aws": {"esm": {"region_name": "mock-region"}}}

    ctx = await hub.idem.acct.ctx(
        path="states.aws.", acct_profile="default", acct_data=PROFILES
    )

    assert ctx.acct.get("region_name") == "mock-region", hub.OPT.acct.extras


@pytest.mark.asyncio
async def test_gather_esm_extras_override(hub):
    """
    Verify that esm region name has priority for esm profiles
    """
    ACCT = """
    aws:
      default:
        aws_access_key_id: my_key_id
        aws_secret_access_key: my_secret_key
        esm:
          foo: bar
        # region_name
    """

    PROFILES = dict(profiles=yaml.safe_load(ACCT))

    CONFIG = b"""
    acct:
      extras:
        aws:
          region_name: overridden
          esm:
            region_name: mock-region
    """

    with tempfile.NamedTemporaryFile(suffix=".cfg", delete=True) as fh:
        fh.write(CONFIG)
        fh.flush()

        with mock.patch("sys.argv", ["idem", "state", f"--config={fh.name}"]):
            hub.pop.config.load(
                ["idem", "acct", "rend", "evbus"], "idem", parse_cli=True
            )

    assert hub.OPT.acct.extras == {
        "aws": {"region_name": "overridden", "esm": {"region_name": "mock-region"}}
    }

    ctx = await hub.idem.acct.ctx(
        path="states.aws.", acct_profile="default", acct_data=PROFILES
    )

    assert ctx.acct.get("region_name") == "mock-region", hub.OPT.acct.extras


@pytest.mark.asyncio
async def test_gather_no_esm(hub):
    """
    Verify that when no esm profile is used, the normal config region_name is used, not the esm region_name
    """
    ACCT = """
    aws:
      default:
        aws_access_key_id: my_key_id
        aws_secret_access_key: my_secret_key
        # region_name
    """

    PROFILES = dict(profiles=yaml.safe_load(ACCT))

    CONFIG = b"""
    acct:
      extras:
        aws:
          region_name: mock-region
          esm:
            region_name: overridden
    idem:
      esm_profile: null
    """

    with tempfile.NamedTemporaryFile(suffix=".cfg", delete=True) as fh:
        fh.write(CONFIG)
        fh.flush()

        with mock.patch("sys.argv", ["idem", "state", f"--config={fh.name}"]):
            hub.pop.config.load(
                ["idem", "acct", "rend", "evbus"], "idem", parse_cli=True
            )

    assert hub.OPT.acct.extras == {
        "aws": {"region_name": "mock-region", "esm": {"region_name": "overridden"}}
    }

    ctx = await hub.idem.acct.ctx(
        path="states.aws.", acct_profile="default", acct_data=PROFILES
    )

    assert ctx.acct.get("region_name") == "mock-region", hub.OPT.acct.extras


@pytest.mark.asyncio
async def test_gather_esm_extras(hub):
    """
    Verify that when a profile is specified as an esm-profile it's region is collected from extras
    """
    ACCT = """
    aws:
      default:
        aws_access_key_id: my_key_id
        aws_secret_access_key: my_secret_key
        # region_name
    """

    PROFILES = dict(profiles=yaml.safe_load(ACCT))

    CONFIG = b"""
    acct:
      extras:
        aws:
          region_name: unused
          esm:
            region_name: mock-region
    idem:
      esm_profile: default
    """

    with tempfile.NamedTemporaryFile(suffix=".cfg", delete=True) as fh:
        fh.write(CONFIG)
        fh.flush()

        with mock.patch("sys.argv", ["idem", "state", f"--config={fh.name}"]):
            hub.pop.config.load(
                ["idem", "acct", "rend", "evbus"], "idem", parse_cli=True
            )

    assert hub.OPT.acct.extras == {
        "aws": {"region_name": "unused", "esm": {"region_name": "mock-region"}}
    }
    assert hub.OPT.idem.esm_profile == "default"

    ctx = await hub.idem.acct.ctx(
        path="states.aws.", acct_profile="default", acct_data=PROFILES
    )

    assert ctx.acct.get("region_name") == "mock-region", hub.OPT.acct.extras
