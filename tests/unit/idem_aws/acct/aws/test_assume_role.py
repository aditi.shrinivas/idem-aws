from unittest import mock

import pytest

PROFILES = {
    "aws": {
        "default": {
            "aws_access_key_id": "my_key_id",
            "aws_secret_access_key": "my_secret_key",
            "region": "us-east-1",
            "assume_role": {
                "role_arn": "arn:aws:iam::999999999:role/xacct/administrator",
                "external_id": "test",
                "role_session_name": "Idem",
            },
            "esm": {"unmodified_key": "unmodified_value"},
        },
    }
}


@pytest.mark.asyncio
async def test_gather(hub, mock_hub):
    hub.tool.boto3.client.exec = mock_hub.tool.boto3.client.exec
    hub.tool.boto3.client.exec.return_value = {
        "Credentials": {
            "AccessKeyId": "my_new_key_id",
            "SecretAccessKey": "my_new_key",
            "SessionToken": "my_new_token",
        }
    }

    with mock.patch("botocore.config.Config") as config:
        p1 = await hub.acct.init.gather(
            subs=["aws"], profile="default", profiles=PROFILES
        )
        mock_hub.tool.boto3.client.exec.assert_called_once_with(
            {
                "acct": {
                    "region_name": "us-east-1",
                    "verify": None,
                    "use_ssl": True,
                    "endpoint_url": None,
                    "aws_access_key_id": "my_key_id",
                    "aws_secret_access_key": "my_secret_key",
                    "aws_session_token": None,
                    "config": config(),
                },
                "test": False,
            },
            "sts",
            "assume_role",
            RoleArn="arn:aws:iam::999999999:role/xacct/administrator",
            RoleSessionName="Idem",
            ExternalId="test",
        )

    assert p1["aws_access_key_id"] == "my_new_key_id"
    assert p1["aws_secret_access_key"] == "my_new_key"
    assert p1["aws_session_token"] == "my_new_token"
    assert p1["region"] == "us-east-1"
    assert p1["esm"] == {"unmodified_key": "unmodified_value"}
