async def test_update_metric_tags(hub, mock_hub, ctx):
    ctx["test"] = True
    mock_hub.exec.aws.cloudwatch.metric_alarm.update_metric_tags = (
        hub.exec.aws.cloudwatch.metric_alarm.update_metric_tags
    )
    mock_hub.tool.aws.tag_utils.convert_tag_list_to_dict = (
        hub.tool.aws.tag_utils.convert_tag_list_to_dict
    )
    mock_hub.tool.aws.tag_utils.diff_tags_dict = hub.tool.aws.tag_utils.diff_tags_dict
    alarm_arn = "arn:aws:autoscaling:region:xxxxxx:scalingPolicy:xxxx4-xxx-xx-xxx-xxxx:resource/alarm1"

    # case when new_tags is None
    old_tags = {"k1": "v1", "k2": "v2"}
    new_tags = None
    ret = await mock_hub.exec.aws.cloudwatch.metric_alarm.update_metric_tags(
        ctx, alarm_arn, old_tags, new_tags
    )

    assert ret["result"]
    assert ret["ret"] == old_tags

    # case when new_tags is not None
    old_tags = {"k1": "v1", "k2": "v2"}
    new_tags = {"k1": "v1", "k2": "v2"}
    ret = await mock_hub.exec.aws.cloudwatch.metric_alarm.update_metric_tags(
        ctx, alarm_arn, old_tags, new_tags
    )

    assert ret["result"]
    assert ret["ret"] == new_tags

    # case when old_tags and new_tags are None
    old_tags = None
    new_tags = None

    ret = await mock_hub.exec.aws.cloudwatch.metric_alarm.update_metric_tags(
        ctx, alarm_arn, old_tags, new_tags
    )

    assert ret["result"]
    assert ret["ret"] == {}

    # case when new_tags is empty
    old_tags = {"k1": "v1", "k2": "v2"}
    new_tags = {}

    ret = await mock_hub.exec.aws.cloudwatch.metric_alarm.update_metric_tags(
        ctx, alarm_arn, old_tags, new_tags
    )

    assert ret["result"]
    assert ret["ret"] == {}
