import copy
import uuid
from collections import ChainMap

import pytest


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
async def test_hosted_zone(hub, ctx):
    # Create hosted_zone
    hosted_zone_name = "idem-test-hosted-zone-" + str(uuid.uuid4()) + ".com"
    caller_reference = "caller_reference-" + str(uuid.uuid4())
    hosted_zone_temp_name = "hosted_zone/" + str(uuid.uuid4())
    hosted_zone_comment = "new hosted zone"
    tags = [{"Key": "Name", "Value": hosted_zone_name}]
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await hub.states.aws.route53.hosted_zone.present(
        test_ctx,
        name=hosted_zone_temp_name,
        hosted_zone_name=hosted_zone_name,
        caller_reference=caller_reference,
        config={"Comment": hosted_zone_comment},
        tags=tags,
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.would_create_comment(
            resource_type="aws.route53.hosted_zone", name=hosted_zone_temp_name
        )[0]
        in ret["comment"]
    )
    assert not ret["old_state"] and ret["new_state"]
    resource = ret.get("new_state")
    assert resource["tags"]
    assert hub.tool.aws.state_comparison_utils.are_lists_identical(
        hub.tool.aws.tag_utils.convert_tag_dict_to_list(resource["tags"]), tags
    )
    assert hosted_zone_name in resource["hosted_zone_name"]
    assert "Comment" in resource.get("config")
    assert hosted_zone_comment == resource.get("config").get("Comment")

    ret = await hub.states.aws.route53.hosted_zone.present(
        ctx,
        name=hosted_zone_temp_name,
        hosted_zone_name=hosted_zone_name,
        caller_reference=caller_reference,
        config={"Comment": hosted_zone_comment},
        tags=tags,
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.create_comment(
            resource_type="aws.route53.hosted_zone", name=hosted_zone_temp_name
        )[0]
        in ret["comment"]
    )
    assert not ret["old_state"] and ret["new_state"]
    resource = ret.get("new_state")
    assert resource["tags"]
    assert hub.tool.aws.state_comparison_utils.are_lists_identical(
        hub.tool.aws.tag_utils.convert_tag_dict_to_list(resource["tags"]), tags
    )
    assert hosted_zone_name + "." == resource["hosted_zone_name"]
    assert "Comment" in resource.get("config")
    assert hosted_zone_comment == resource.get("config").get("Comment")

    created_hosted_zone_id = resource.get("resource_id")

    # verify that created hosted_zone is present
    describe_ret = await hub.states.aws.route53.hosted_zone.describe(ctx)
    assert created_hosted_zone_id in describe_ret
    # Verify that the describe output format is correct
    assert "aws.route53.hosted_zone.present" in describe_ret.get(created_hosted_zone_id)
    described_resource = describe_ret.get(created_hosted_zone_id).get(
        "aws.route53.hosted_zone.present"
    )
    described_resource_map = dict(ChainMap(*described_resource))
    assert "resource_id" in described_resource_map
    assert "config" in described_resource_map
    assert not described_resource_map["config"]["PrivateZone"]
    assert hosted_zone_comment == described_resource_map["config"]["Comment"]
    assert hosted_zone_name + "." == described_resource_map.get("hosted_zone_name")

    # update tags
    tags.append(
        {
            "Key": f"idem-test-hosted-zone-key-{str(uuid.uuid4())}",
            "Value": f"idem-test-hosted-zone-value-{str(uuid.uuid4())}",
        }
    )
    hosted_zone_updated_comment = "hosted_zone updated"
    # update tags and hosted zone comment with test flag
    ret = await hub.states.aws.route53.hosted_zone.present(
        test_ctx,
        name=hosted_zone_temp_name,
        resource_id=created_hosted_zone_id,
        hosted_zone_name=hosted_zone_name,
        config={"Comment": hosted_zone_updated_comment},
        caller_reference=caller_reference,
        tags=tags,
    )

    assert ret["result"], ret["comment"]
    assert (
        f"Would update aws.route53.hostedzone '{hosted_zone_temp_name}'"
        in ret["comment"]
    )
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert resource["tags"]
    assert 2 == len(resource["tags"])
    assert hub.tool.aws.state_comparison_utils.are_lists_identical(
        tags, hub.tool.aws.tag_utils.convert_tag_dict_to_list(resource.get("tags"))
    )
    assert hosted_zone_name + "." == resource["hosted_zone_name"]
    assert "Comment" in resource.get("config")
    assert hosted_zone_updated_comment == resource.get("config").get("Comment")

    # update tags and hosted zone comment in real
    ret = await hub.states.aws.route53.hosted_zone.present(
        ctx,
        name=hosted_zone_temp_name,
        resource_id=created_hosted_zone_id,
        hosted_zone_name=hosted_zone_name,
        caller_reference=caller_reference,
        tags=tags,
    )

    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert resource["tags"]
    assert 2 == len(resource["tags"])
    assert hub.tool.aws.state_comparison_utils.are_lists_identical(
        tags, hub.tool.aws.tag_utils.convert_tag_dict_to_list(resource.get("tags"))
    )
    assert hosted_zone_name + "." == resource["hosted_zone_name"]
    assert "Comment" in resource.get("config")
    assert hosted_zone_comment == resource.get("config").get("Comment")

    # updating comment is not supported in localstack.
    # It gives error ('ClientError: An error occurred (500)
    # when calling the UpdateHostedZoneComment operation (reached max retries: 4)
    if not hub.tool.utils.is_running_localstack(ctx):
        # update hosted zone comment in real
        ret = await hub.states.aws.route53.hosted_zone.present(
            ctx,
            name=hosted_zone_temp_name,
            resource_id=created_hosted_zone_id,
            hosted_zone_name=hosted_zone_name,
            config={"Comment": hosted_zone_updated_comment},
            caller_reference=caller_reference,
            tags=tags,
        )

        assert ret["result"], ret["comment"]
        assert ret.get("old_state") and ret.get("new_state")
        resource = ret.get("new_state")
        assert resource["tags"]
        assert 2 == len(resource["tags"])
        assert hub.tool.aws.state_comparison_utils.are_lists_identical(
            tags, hub.tool.aws.tag_utils.convert_tag_dict_to_list(resource.get("tags"))
        )
        assert hosted_zone_name + "." == resource["hosted_zone_name"]
        assert "Comment" in resource.get("config")
        assert hosted_zone_updated_comment == resource.get("config").get("Comment")

    # Search for hosted_zone which is present
    search_ret = await hub.states.aws.route53.hosted_zone.search(
        ctx,
        name=f"{hosted_zone_name}-search",
        hosted_zone_name=hosted_zone_name + ".",
        private_zone=False,
        tags=tags,
    )
    assert search_ret["result"], search_ret["comment"]
    assert search_ret.get("old_state") and search_ret.get("new_state")
    resource = search_ret.get("new_state")
    assert f"{hosted_zone_name}-search" == resource.get("name")
    assert resource.get("config")
    if not hub.tool.utils.is_running_localstack(ctx):
        assert hosted_zone_updated_comment == resource["config"].get("Comment")
    else:
        assert hosted_zone_comment == resource["config"].get("Comment")
    assert not resource["config"].get("PrivateZone")
    assert hosted_zone_name + "." == resource.get("hosted_zone_name")

    # Search for hosted_zone with resource_id,directly returns hosted zone without checking other filters
    search_ret = await hub.states.aws.route53.hosted_zone.search(
        ctx,
        name=f"{hosted_zone_name}-search",
        resource_id=created_hosted_zone_id,
        private_zone=True,
        tags=tags,
    )
    assert search_ret["result"], search_ret["comment"]
    assert search_ret.get("old_state") and search_ret.get("new_state")
    resource = search_ret.get("new_state")
    assert f"{hosted_zone_name}-search" == resource.get("name")
    assert resource.get("config")
    if not hub.tool.utils.is_running_localstack(ctx):
        assert hosted_zone_updated_comment == resource["config"].get("Comment")
    else:
        assert hosted_zone_comment == resource["config"].get("Comment")
    assert not resource["config"].get("PrivateZone")
    assert hosted_zone_name + "." == resource.get("hosted_zone_name")

    # Search for the hosted_zone which is not present
    search_ret = await hub.states.aws.route53.hosted_zone.search(
        ctx,
        name=f"{hosted_zone_name}-search",
        hosted_zone_name=hosted_zone_name,
        private_zone=True,
        tags=tags,
    )
    assert search_ret["result"], search_ret["comment"]
    assert not search_ret.get("old_state") and not search_ret.get("new_state")

    # Delete hosted zone with test flag
    ret = await hub.states.aws.route53.hosted_zone.absent(
        test_ctx,
        name=hosted_zone_temp_name,
        resource_id=created_hosted_zone_id,
    )
    assert ret["result"] and ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert (
        hub.tool.aws.comment_utils.would_delete_comment(
            resource_type="aws.route53.hosted_zone", name=hosted_zone_temp_name
        )[0]
        in ret["comment"]
    )

    # Delete instance
    ret = await hub.states.aws.route53.hosted_zone.absent(
        ctx,
        name=hosted_zone_temp_name,
        resource_id=created_hosted_zone_id,
    )

    assert ret["result"] and ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert (
        hub.tool.aws.comment_utils.delete_comment(
            resource_type="aws.route53.hosted_zone", name=hosted_zone_temp_name
        )[0]
        in ret["comment"]
    )

    # Deleting the same instance again (deleted state) will not invoke delete on AWS side.
    ret = await hub.states.aws.route53.hosted_zone.absent(
        ctx,
        name=hosted_zone_temp_name,
        resource_id=created_hosted_zone_id,
    )

    assert ret["result"] and ret["comment"]
    assert not ret.get("old_state") and not ret.get("new_state")
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type="aws.route53.hosted_zone", name=hosted_zone_temp_name
        )[0]
        in ret["comment"]
    )


@pytest.mark.asyncio
async def test_hosted_zone_absent_with_none_resource_id(hub, ctx):
    name = "idem-test-hosted-zone-" + str(uuid.uuid4()) + ".com"
    # Delete hosted zone with resource_id as None. Result in no-op.
    ret = await hub.states.aws.route53.hosted_zone.absent(
        ctx,
        name=name,
        resource_id=None,
    )
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type="aws.route53.hosted_zone", name=name
        )[0]
        in ret["comment"]
    )
