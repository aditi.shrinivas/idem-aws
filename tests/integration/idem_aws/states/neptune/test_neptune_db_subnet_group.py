import copy
import uuid
from collections import ChainMap

import pytest


@pytest.mark.localstack(pro=True)
@pytest.mark.asyncio
async def test_db_subnet_group(hub, ctx, aws_ec2_subnet, aws_ec2_subnet_2):
    # To skip running on localstack as localstack/pro is not able to support modifying the resource, test is working with real aws
    # RDS's db_subnet_group also has the same problem and is skipping the test on localtack
    if hub.tool.utils.is_running_localstack(ctx):
        return
    # Create DB Subnet Group
    db_subnet_group_name = "idem-test-neptune-db-subnet-group-" + str(uuid.uuid4())
    tags = [
        {"Key": "Name", "Value": db_subnet_group_name},
    ]
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True

    comment_utils_kwargs = {
        "resource_type": "aws.neptune.db_subnet_group",
        "name": db_subnet_group_name,
    }
    created_message = hub.tool.aws.comment_utils.create_comment(**comment_utils_kwargs)[
        0
    ]
    would_create_message = hub.tool.aws.comment_utils.would_create_comment(
        **comment_utils_kwargs
    )[0]
    deleted_message = hub.tool.aws.comment_utils.delete_comment(**comment_utils_kwargs)[
        0
    ]
    would_delete_message = hub.tool.aws.comment_utils.would_delete_comment(
        **comment_utils_kwargs
    )[0]
    already_absent_message = hub.tool.aws.comment_utils.already_absent_comment(
        **comment_utils_kwargs
    )[0]

    subnet_ids = [aws_ec2_subnet.get("SubnetId"), aws_ec2_subnet_2.get("SubnetId")]
    # --test create
    ret = await hub.states.aws.neptune.db_subnet_group.present(
        test_ctx,
        name=db_subnet_group_name,
        db_subnet_group_description="For testing idem plugin",
        subnet_ids=subnet_ids,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    assert would_create_message in ret["comment"]
    resource = ret["new_state"]
    assert resource["name"] == db_subnet_group_name
    assert resource["db_subnet_group_description"] == "For testing idem plugin"
    assert tags == resource["tags"]
    assert resource["subnet_ids"]
    assert len(resource["subnet_ids"]) == 2
    assert set(subnet_ids) == set(resource["subnet_ids"])

    # Create DB Subnet Group real
    ret = await hub.states.aws.neptune.db_subnet_group.present(
        ctx,
        name=db_subnet_group_name,
        db_subnet_group_description="For testing idem plugin",
        subnet_ids=subnet_ids,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    assert created_message in ret["comment"]
    resource = ret["new_state"]
    assert resource["name"] == db_subnet_group_name
    assert resource["db_subnet_group_description"] == "For testing idem plugin"
    assert tags == resource["tags"]
    assert resource["subnet_ids"]
    assert len(resource["subnet_ids"]) == 2
    assert set(subnet_ids) == set(resource["subnet_ids"])
    resource_id = resource["resource_id"]

    # Describe instance
    describe_ret = await hub.states.aws.neptune.db_subnet_group.describe(ctx)
    assert resource_id in describe_ret
    resource = dict(
        ChainMap(*describe_ret[resource_id].get("aws.neptune.db_subnet_group.present"))
    )
    assert db_subnet_group_name == resource.get("name")
    assert tags == resource["tags"]
    assert resource["db_subnet_group_description"] == "For testing idem plugin"
    assert resource["subnet_ids"]
    assert set(subnet_ids) == set(resource["subnet_ids"])

    # --test update without any changes to tags
    ret = await hub.states.aws.neptune.db_subnet_group.present(
        test_ctx,
        name=db_subnet_group_name,
        resource_id=resource_id,
        db_subnet_group_description="For testing idem plugin. --test.",
        subnet_ids=subnet_ids,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret["new_state"]
    assert resource["db_subnet_group_description"] == "For testing idem plugin. --test."
    assert tags == resource["tags"]

    # update without any changes to tags
    ret = await hub.states.aws.neptune.db_subnet_group.present(
        ctx,
        name=db_subnet_group_name,
        resource_id=resource_id,
        db_subnet_group_description="For testing idem plugin",
        subnet_ids=subnet_ids,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    assert (
        f"aws.neptune.db_subnet_group '{db_subnet_group_name}' already exists"
        in ret["comment"]
    )
    resource = ret["new_state"]
    assert db_subnet_group_name == resource.get("name")
    assert resource["subnet_ids"]
    assert len(resource["subnet_ids"]) == 2
    assert set(subnet_ids) == set(resource["subnet_ids"])
    assert resource["db_subnet_group_description"] == "For testing idem plugin"
    assert tags == resource["tags"]

    # Updating tags
    new_tags = [
        {"Key": "Name", "Value": f"Updated {db_subnet_group_name} during testing"},
    ]

    # --test Run with test as true for real update tags
    ret = await hub.states.aws.neptune.db_subnet_group.present(
        test_ctx,
        name=db_subnet_group_name,
        resource_id=resource_id,
        db_subnet_group_description="For testing idem plugin",
        subnet_ids=subnet_ids,
        tags=new_tags,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    assert ret["new_state"]["tags"] == new_tags

    # Updating tags real
    ret = await hub.states.aws.neptune.db_subnet_group.present(
        ctx,
        name=db_subnet_group_name,
        resource_id=resource_id,
        db_subnet_group_description="For testing idem plugin",
        subnet_ids=subnet_ids,
        tags=new_tags,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    assert ret["new_state"]["tags"] == new_tags
    assert (
        f"Updated aws.neptune.db_subnet_group '{db_subnet_group_name}'"
        in ret["comment"]
    )
    assert ret["new_state"]["subnet_ids"]
    assert set(subnet_ids) == set(ret["new_state"]["subnet_ids"])

    # Describe instance
    describe_ret = await hub.states.aws.neptune.db_subnet_group.describe(ctx)
    assert resource_id in describe_ret
    assert describe_ret.get(resource_id) and describe_ret.get(resource_id).get(
        "aws.neptune.db_subnet_group.present"
    )
    resource = describe_ret.get(resource_id).get("aws.neptune.db_subnet_group.present")
    describe_tags = None
    resource = dict(
        ChainMap(*describe_ret[resource_id].get("aws.neptune.db_subnet_group.present"))
    )
    describe_tags = resource.get("tags")
    assert db_subnet_group_name == resource.get("name")
    assert resource["db_subnet_group_description"] == "For testing idem plugin"
    assert resource["subnet_ids"]
    assert set(subnet_ids) == set(resource["subnet_ids"])
    assert describe_tags == new_tags

    # removing tags and changing description with --test as true.
    ret = await hub.states.aws.neptune.db_subnet_group.present(
        test_ctx,
        name=db_subnet_group_name,
        resource_id=resource_id,
        db_subnet_group_description="For testing idem plugin with changed description",
        subnet_ids=subnet_ids,
        tags=[],
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    assert ret["old_state"]["tags"] == new_tags
    assert ret["new_state"]["tags"] == []
    assert (
        ret["new_state"]["db_subnet_group_description"]
        == "For testing idem plugin with changed description"
    )

    # removing tags and changing description real
    ret = await hub.states.aws.neptune.db_subnet_group.present(
        ctx,
        name=db_subnet_group_name,
        resource_id=resource_id,
        db_subnet_group_description="For testing idem plugin with changed description",
        subnet_ids=subnet_ids,
        tags=[],
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    assert ret["old_state"]["tags"] == new_tags
    assert ret["new_state"]["tags"] == []
    assert (
        ret["new_state"]["db_subnet_group_description"]
        == "For testing idem plugin with changed description"
    )
    assert (
        f"Updated aws.neptune.db_subnet_group '{db_subnet_group_name}'"
        in ret["comment"]
    )

    # Delete instance with --test as true
    ret = await hub.states.aws.neptune.db_subnet_group.absent(
        test_ctx, name=resource_id, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert would_delete_message in ret["comment"]
    resource = ret.get("old_state")
    assert db_subnet_group_name == resource.get("name")
    assert (
        resource["db_subnet_group_description"]
        == "For testing idem plugin with changed description"
    )
    assert set(subnet_ids) == set(resource["subnet_ids"])
    assert not (resource.get("tags"))

    # Delete instance
    ret = await hub.states.aws.neptune.db_subnet_group.absent(
        ctx, name=resource_id, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert deleted_message in ret["comment"]
    resource = ret.get("old_state")
    assert db_subnet_group_name == resource.get("name")
    assert (
        resource["db_subnet_group_description"]
        == "For testing idem plugin with changed description"
    )
    assert set(subnet_ids) == set(resource["subnet_ids"])
    assert not (resource.get("tags"))

    # should not try to delete already deleted or non-existent resource.
    # It should promptly say resource is already absent
    ret = await hub.states.aws.neptune.db_subnet_group.absent(
        ctx, name=resource_id, resource_id=resource_id
    )
    assert already_absent_message in ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
