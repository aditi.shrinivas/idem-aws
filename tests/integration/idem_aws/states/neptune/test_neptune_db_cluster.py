import copy
import uuid
from collections import ChainMap

import pytest


@pytest.mark.localstack(pro=True)
@pytest.mark.asyncio
async def test_db_cluster(hub, ctx):
    # To skip running on localstack as localstack/pro is not able to support aws neptune for tests
    if hub.tool.utils.is_running_localstack(ctx):
        return
    # Create Neptune DB cluster
    db_cluster_name = "idem-test-db-cluster-" + str(uuid.uuid4())
    engine = "aurora"
    master_username = "admin123"
    master_user_password = "abcd1234"
    backup_retention_period = 7
    deletion_protection = True  # we will modify this later as part of test
    tags = [
        {"Key": "Name", "Value": db_cluster_name},
    ]
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True

    comment_utils_kwargs = {
        "resource_type": "aws.neptune.db_cluster",
        "name": db_cluster_name,
    }
    created_message = hub.tool.aws.comment_utils.create_comment(**comment_utils_kwargs)[
        0
    ]
    would_create_message = hub.tool.aws.comment_utils.would_create_comment(
        **comment_utils_kwargs
    )[0]
    deleted_message = hub.tool.aws.comment_utils.delete_comment(**comment_utils_kwargs)[
        0
    ]
    would_delete_message = hub.tool.aws.comment_utils.would_delete_comment(
        **comment_utils_kwargs
    )[0]
    already_absent_message = hub.tool.aws.comment_utils.already_absent_comment(
        **comment_utils_kwargs
    )[0]

    # Create DB Cluster with test flags
    ret = await hub.states.aws.neptune.db_cluster.present(
        test_ctx,
        name=db_cluster_name,
        engine=engine,
        master_username=master_username,
        master_user_password=master_user_password,
        backup_retention_period=backup_retention_period,
        deletion_protection=deletion_protection,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert would_create_message in ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    hub.tool.neptune_db_cluster_test_util.assert_neptune_db_cluster(
        resource,
        db_cluster_name,
        engine,
        master_username,
        backup_retention_period,
        deletion_protection,
        tags,
    )

    # Create DB Cluster real
    ret = await hub.states.aws.neptune.db_cluster.present(
        ctx,
        name=db_cluster_name,
        engine=engine,
        master_username=master_username,
        master_user_password=master_user_password,
        backup_retention_period=backup_retention_period,
        deletion_protection=deletion_protection,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert created_message in ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    hub.tool.neptune_db_cluster_test_util.assert_neptune_db_cluster(
        resource,
        db_cluster_name,
        engine,
        master_username,
        backup_retention_period,
        deletion_protection,
        tags,
    )
    resource_id = resource.get("resource_id")

    # Describe DB Cluster
    describe_ret = await hub.states.aws.neptune.db_cluster.describe(ctx)
    assert resource_id in describe_ret
    resource = dict(
        ChainMap(*describe_ret[resource_id].get("aws.neptune.db_cluster.present"))
    )
    hub.tool.neptune_db_cluster_test_util.assert_neptune_db_cluster(
        resource,
        db_cluster_name,
        engine,
        master_username,
        backup_retention_period,
        deletion_protection,
        tags,
    )

    # Updating the db_cluster with test flag
    tags.append(
        {
            "Key": f"idem-test-db-cluster-key-{str(uuid.uuid4())}",
            "Value": f"idem-test-db-cluster-value-{str(uuid.uuid4())}",
        }
    )
    backup_retention_period = 5
    deletion_protection = False
    await hub.tool.neptune_db_cluster_test_util.check_update_cluster(
        ctx=test_ctx,  #  pass test flag
        db_cluster_name=db_cluster_name,
        resource_id=resource_id,
        engine=engine,
        master_username=master_username,
        master_user_password=master_user_password,
        backup_retention_period=backup_retention_period,
        deletion_protection=deletion_protection,
        tags=tags,
    )

    # Updating the db_cluster (real)
    await hub.tool.neptune_db_cluster_test_util.check_update_cluster(
        ctx=ctx,  #  pass real flag
        db_cluster_name=db_cluster_name,
        resource_id=resource_id,
        engine=engine,
        master_username=master_username,
        master_user_password=master_user_password,
        backup_retention_period=backup_retention_period,
        deletion_protection=deletion_protection,
        tags=tags,
    )

    # Test deleting tags
    tags = [tags[0]]
    ret = await hub.states.aws.neptune.db_cluster.present(
        ctx,
        name=db_cluster_name,
        resource_id=resource_id,
        engine=engine,
        master_username=master_username,
        master_user_password=master_user_password,
        backup_retention_period=backup_retention_period,
        deletion_protection=deletion_protection,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert hub.tool.aws.state_comparison_utils.are_lists_identical(
        tags, resource.get("tags")
    )

    # Delete cluster with test flag
    ret = await hub.states.aws.neptune.db_cluster.absent(
        test_ctx,
        name=db_cluster_name,
        resource_id=resource_id,
        skip_final_snapshot=True,
    )
    assert ret["result"], ret["comment"]
    assert would_delete_message in ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    resource = ret.get("old_state")
    hub.tool.neptune_db_cluster_test_util.assert_neptune_db_cluster(
        resource,
        db_cluster_name,
        engine,
        master_username,
        backup_retention_period,
        deletion_protection,
        tags,
    )

    # Delete without resource id, using --test, it should return already absent method
    ret = await hub.states.aws.neptune.db_cluster.absent(
        test_ctx, name=db_cluster_name, skip_final_snapshot=True
    )
    assert ret["result"], ret["comment"]
    assert already_absent_message in ret["comment"]
    assert (not ret.get("old_state")) and (not ret.get("new_state"))

    # Delete without resource id, without using --test, it should return already absent method
    ret = await hub.states.aws.neptune.db_cluster.absent(
        ctx, name=db_cluster_name, skip_final_snapshot=True
    )
    assert ret["result"], ret["comment"]
    assert already_absent_message in ret["comment"]
    assert (not ret.get("old_state")) and (not ret.get("new_state"))

    # Delete cluster real
    ret = await hub.states.aws.neptune.db_cluster.absent(
        ctx,
        name=db_cluster_name,
        resource_id=resource_id,
        skip_final_snapshot=True,
    )
    assert ret["result"], ret["comment"]
    assert deleted_message in ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    resource = ret.get("old_state")
    hub.tool.neptune_db_cluster_test_util.assert_neptune_db_cluster(
        resource,
        db_cluster_name,
        engine,
        master_username,
        backup_retention_period,
        deletion_protection,
        tags,
    )

    # Should not try to delete already deleted or non-existent resource.
    # It should promptly say resource is already absent
    ret = await hub.states.aws.neptune.db_cluster.absent(
        ctx, name=db_cluster_name, resource_id=resource_id, skip_final_snapshot=True
    )
    assert ret["result"], ret["comment"]
    assert already_absent_message in ret["comment"]
    assert (not ret.get("old_state")) and (not ret.get("new_state"))
