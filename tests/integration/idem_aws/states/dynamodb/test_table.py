import copy
import uuid

import pytest


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
async def test_dynamodb_table(hub, ctx, aws_kms_key):
    # Create dynamodb table
    table_temp_name = "idem-test-dynamodb-table" + str(uuid.uuid4())
    tags = {"Name": table_temp_name}
    attribute_definitions = [
        {
            "AttributeName": "Artist",
            "AttributeType": "S",
        },
        {
            "AttributeName": "SongTitle",
            "AttributeType": "S",
        },
    ]
    key_schema = [
        {
            "AttributeName": "Artist",
            "KeyType": "HASH",
        },
        {
            "AttributeName": "SongTitle",
            "KeyType": "RANGE",
        },
    ]
    provisioned_throughput = {
        "ReadCapacityUnits": 123,
        "WriteCapacityUnits": 123,
    }
    billing_mode = "PROVISIONED"
    stream_specification = {"StreamEnabled": True, "StreamViewType": "NEW_IMAGE"}
    sse_specification = {
        "Enabled": True,
        "SSEType": "KMS",
        "KMSMasterKeyId": aws_kms_key.get("resource_id"),
    }
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True

    # Idem state --test
    ret = await hub.states.aws.dynamodb.table.present(
        test_ctx,
        name=table_temp_name,
        attribute_definitions=attribute_definitions,
        key_schema=key_schema,
        billing_mode=billing_mode,
        provisioned_throughput=provisioned_throughput,
        stream_specification=stream_specification,
        sse_specification=sse_specification,
        tags=tags,
    )

    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    assert f"Would create aws.dynamodb.table '{table_temp_name}'" in ret["comment"]
    resource = ret.get("new_state")
    assert table_temp_name == resource.get("name")
    assert resource.get("attribute_definitions") == attribute_definitions
    assert resource.get("key_schema") == key_schema
    assert tags == resource.get("tags")
    assert resource.get("provisioned_throughput")
    assert resource.get("sse_specification")
    assert resource.get("stream_specification")

    # Create Dynamo DB Table.
    ret = await hub.states.aws.dynamodb.table.present(
        ctx,
        name=table_temp_name,
        attribute_definitions=attribute_definitions,
        key_schema=key_schema,
        billing_mode=billing_mode,
        provisioned_throughput=provisioned_throughput,
        stream_specification=stream_specification,
        sse_specification=sse_specification,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert table_temp_name == resource.get("name")
    assert resource.get("attribute_definitions") == attribute_definitions
    assert resource.get("key_schema") == key_schema
    assert tags == resource.get("tags")

    assert resource.get("provisioned_throughput")
    assert resource.get("provisioned_throughput").get(
        "ReadCapacityUnits"
    ) == provisioned_throughput.get("ReadCapacityUnits")
    assert resource.get("provisioned_throughput").get(
        "WriteCapacityUnits"
    ) == provisioned_throughput.get("WriteCapacityUnits")

    # Latest versions of localstack is not passing this field in describe output.
    # Asserting these values only for AWS runs.
    if not hub.tool.utils.is_running_localstack(ctx):
        assert resource.get("sse_specification")
        assert (
            resource.get("sse_specification").get("SSEType")
            == sse_specification["SSEType"]
        )
        assert resource.get("sse_specification").get("KMSMasterKeyArn")

    assert resource.get("stream_specification")
    assert (
        resource.get("stream_specification").get("StreamViewType")
        == stream_specification["StreamViewType"]
    )
    assert (
        resource.get("stream_specification").get("StreamEnabled")
        == stream_specification["StreamEnabled"]
    )

    tags = {"Name": table_temp_name, "new_tag": "new_value"}

    # Idem state --test. Update DynamoDB table
    ret = await hub.states.aws.dynamodb.table.present(
        test_ctx,
        name=table_temp_name,
        resource_id=table_temp_name,
        attribute_definitions=attribute_definitions,
        key_schema=key_schema,
        provisioned_throughput=provisioned_throughput,
        tags=tags,
        billing_mode="PAY_PER_REQUEST",
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert resource.get("provisioned_throughput")
    assert tags == resource.get("tags")

    provisioned_throughput = {
        "ReadCapacityUnits": 110,
        "WriteCapacityUnits": 110,
    }
    # update dynamoDB table
    ret = await hub.states.aws.dynamodb.table.present(
        ctx,
        name=table_temp_name,
        resource_id=table_temp_name,
        attribute_definitions=attribute_definitions,
        key_schema=key_schema,
        provisioned_throughput=provisioned_throughput,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert resource.get("provisioned_throughput")
    assert tags == resource.get("tags")

    provisioned_throughput = {
        "ReadCapacityUnits": 110,
        "WriteCapacityUnits": 110,
    }
    # Idem state --test. Update DynamoDB table. This time with no tags in update. It should return old tags only.
    ret = await hub.states.aws.dynamodb.table.present(
        test_ctx,
        name=table_temp_name,
        resource_id=table_temp_name,
        attribute_definitions=attribute_definitions,
        key_schema=key_schema,
        provisioned_throughput=provisioned_throughput,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    new_resource = ret.get("new_state")
    old_resource = ret.get("old_state")
    assert new_resource.get("tags") == old_resource.get("tags")

    provisioned_throughput = {
        "ReadCapacityUnits": 100,
        "WriteCapacityUnits": 100,
    }
    # Update DynamoDB table. This time with no tags in update. It should return old tags only.
    ret = await hub.states.aws.dynamodb.table.present(
        ctx,
        name=table_temp_name,
        resource_id=table_temp_name,
        attribute_definitions=attribute_definitions,
        key_schema=key_schema,
        provisioned_throughput=provisioned_throughput,
        billing_mode=billing_mode,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    new_resource = ret.get("new_state")
    old_resource = ret.get("old_state")
    assert new_resource.get("tags") == old_resource.get("tags")
    assert len(old_resource.get("tags")) == 2 and len(new_resource.get("tags")) == 2
    assert old_resource.get("tags") == new_resource.get("tags")

    # Describe dynamodb_table
    describe_ret = await hub.states.aws.dynamodb.table.describe(ctx)
    assert describe_ret[table_temp_name]
    resource = describe_ret[table_temp_name]["aws.dynamodb.table.present"][0]
    assert tags == resource.get("tags")

    # Idem state --test. Delete dynamodb_table
    ret = await hub.states.aws.dynamodb.table.absent(
        test_ctx,
        name=table_temp_name,
        resource_id=table_temp_name,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and (not ret.get("new_state"))

    # Delete dynamodb_table
    ret = await hub.states.aws.dynamodb.table.absent(
        ctx,
        name=table_temp_name,
        resource_id=table_temp_name,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state")
    resource = ret.get("old_state")
    assert table_temp_name == resource.get("name")
    assert resource.get("attribute_definitions") == attribute_definitions
    assert resource.get("key_schema") == key_schema
    assert len(old_resource.get("tags")) == 2 and len(new_resource.get("tags")) == 2
    assert resource.get("provisioned_throughput")
    assert resource.get("provisioned_throughput").get(
        "ReadCapacityUnits"
    ) == provisioned_throughput.get("ReadCapacityUnits")
    assert resource.get("provisioned_throughput").get(
        "WriteCapacityUnits"
    ) == provisioned_throughput.get("WriteCapacityUnits")
    # Latest versions of localstack is not passing this field in describe output.
    # Asserting these values only for AWS runs.
    if not hub.tool.utils.is_running_localstack(ctx):
        assert resource.get("sse_specification")
        assert (
            resource.get("sse_specification").get("SSEType")
            == sse_specification["SSEType"]
        )
        assert resource.get("sse_specification").get("KMSMasterKeyArn")

    assert resource.get("stream_specification")
    assert (
        resource.get("stream_specification").get("StreamViewType")
        == stream_specification["StreamViewType"]
    )
    assert (
        resource.get("stream_specification").get("StreamEnabled")
        == stream_specification["StreamEnabled"]
    )

    # Trying to delete an already deleted resource. It should say resource already got deleted.
    ret = await hub.states.aws.dynamodb.table.absent(
        ctx,
        name=table_temp_name,
        resource_id=table_temp_name,
    )
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type="aws.dynamodb.table", name=table_temp_name
        )[0]
        in ret["comment"]
    )
