import copy
import time
import uuid
from collections import ChainMap

import pytest


@pytest.mark.asyncio
async def test_instance_profile(hub, ctx, aws_iam_role):
    # Updating tags on existing instance profile is not being tested,
    # since it is failing against localstack but passing with a real AWS

    # Create IAM instance_profile
    instance_profile_temp_name = "idem-test-instance_profile-" + str(int(time.time()))
    path = "/test/"
    tags = {"Name": instance_profile_temp_name}
    ret = await hub.states.aws.iam.instance_profile.present(
        ctx,
        name=instance_profile_temp_name,
        path=path,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource_id = ret.get("new_state").get("resource_id", None)
    assert resource_id is not None
    resource = ret.get("new_state")
    assert path == resource.get("path")
    assert instance_profile_temp_name == resource.get("name")
    assert tags == resource.get("tags")

    # Try to create the same instance profile again
    ret = await hub.states.aws.iam.instance_profile.present(
        ctx,
        name=instance_profile_temp_name,
        resource_id=resource_id,
        path=path,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    assert ret.get("old_state") == ret.get("new_state")
    assert "already exists" in str(ret["comment"]), ret["comment"]

    # Set the role on the instance_profile
    role_1 = aws_iam_role.get("name")
    ret = await hub.states.aws.iam.instance_profile.present(
        ctx,
        name=instance_profile_temp_name,
        resource_id=resource_id,
        roles=[{"RoleName": role_1}],
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert role_1 == resource.get("roles")[0].get("RoleName")

    # Testing with test flag - start
    # Create instance profile with role with test flag
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await hub.states.aws.iam.instance_profile.present(
        test_ctx,
        name=instance_profile_temp_name + "-test",
        roles=[{"RoleName": "test-role"}],
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert "test-role" == resource.get("roles")[0].get("RoleName")
    assert instance_profile_temp_name + "-test" == resource.get("name")
    assert tags == resource.get("tags")

    # Update tags and change role on existing instance profile with test flag
    test_tags = {"Name": instance_profile_temp_name + "test"}
    ret = await hub.states.aws.iam.instance_profile.present(
        test_ctx,
        name=instance_profile_temp_name,
        resource_id=resource_id,
        roles=[{"RoleName": "test-role-new"}],
        tags=test_tags,
    )
    assert ret["result"], ret["comment"]
    assert "already exists" in str(ret["comment"]), ret["comment"]
    assert "Would update tags" in str(ret["comment"]), ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert "test-role-new" == resource.get("roles")[0].get("RoleName")
    assert test_tags == resource.get("tags")

    # Update tags and remove role with test flag
    ret = await hub.states.aws.iam.instance_profile.present(
        test_ctx,
        name=instance_profile_temp_name,
        resource_id=resource_id,
        roles=[],
        tags=test_tags,
    )
    assert ret["result"], ret["comment"]
    assert "already exists" in str(ret["comment"]), ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert [] == resource.get("roles")
    assert test_tags == resource.get("tags")

    # Delete instance profile with test flag
    ret = await hub.states.aws.iam.instance_profile.absent(
        test_ctx,
        name=instance_profile_temp_name,
        resource_id=resource_id,
    )
    assert ret["result"], ret["comment"]
    assert "Would delete" in str(ret["comment"]), ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    # Testing with test flag - end

    # Create another role
    role_2 = "idem-test-role-" + str(uuid.uuid4())
    assume_role_policy_document = '{"Version": "2012-10-17","Statement": {"Effect": "Allow","Action": "sts:AssumeRole","Principal": {"Service": "spot.amazonaws.com"}}}'
    ret = await hub.states.aws.iam.role.present(
        ctx, name=role_2, assume_role_policy_document=assume_role_policy_document
    )
    assert ret["result"], ret["comment"]
    assert role_2 == ret["new_state"]["name"]

    # Update instance profile with the second role
    ret = await hub.states.aws.iam.instance_profile.present(
        ctx,
        name=instance_profile_temp_name,
        resource_id=resource_id,
        roles=[{"RoleName": role_2}],
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    resource = ret["new_state"]
    assert role_2 == resource["roles"][0]["RoleName"]

    # Describe IAM instance_profile
    describe_ret = await hub.states.aws.iam.instance_profile.describe(ctx)
    resource_key = f"iam-instance-profile-{resource_id}"
    assert resource_key in describe_ret
    assert "aws.iam.instance_profile.present" in describe_ret.get(resource_key)
    resource = describe_ret[resource_key]["aws.iam.instance_profile.present"]
    described_resource_map = dict(ChainMap(*resource))
    assert instance_profile_temp_name == described_resource_map.get("name")
    assert resource_id == described_resource_map.get("resource_id")
    assert tags == described_resource_map.get("tags")

    # Search IAM instance_profile
    search_ret = await hub.states.aws.iam.instance_profile.search(
        ctx, name=instance_profile_temp_name
    )
    assert search_ret["result"], search_ret["comment"]

    # old_state and new_state not None
    assert search_ret.get("old_state") and search_ret.get("new_state")
    # old_state and new state values are equals
    assert search_ret.get("new_state") == search_ret.get("old_state")
    resource = search_ret.get("new_state")
    assert resource_id == resource.get("resource_id")
    assert instance_profile_temp_name in resource.get("arn")
    assert path == resource.get("path")
    assert role_2 in resource.get("roles")[0].get("RoleArn")
    assert resource.get("roles")[0].get(
        "RoleId"
    )  # This is generated value, so asserting for not None
    assert role_2 == resource.get("roles")[0].get("RoleName")
    assert instance_profile_temp_name == resource.get("name")
    assert tags == resource.get("tags")

    # Test Search IAM instance_profile not present
    search_ret = await hub.states.aws.iam.instance_profile.search(
        ctx, name="non-existence"
    )
    assert not search_ret["result"], search_ret["comment"]
    assert (
        f"Unable to find aws.iam.instance_profile 'non-existence'"
        in search_ret["comment"]
    )
    assert not search_ret.get("old_state") and not search_ret.get("new_state")

    # Remove role from instance profile
    ret = await hub.states.aws.iam.instance_profile.present(
        ctx,
        name=instance_profile_temp_name,
        resource_id=resource_id,
        roles=[],
    )
    assert ret["result"], ret["comment"]
    resource = ret["new_state"]
    assert not resource.get("roles")

    # Delete IAM instance_profile
    ret = await hub.states.aws.iam.instance_profile.absent(
        ctx,
        name=instance_profile_temp_name,
        resource_id=resource_id,
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and not ret["new_state"]

    # Delete IAM instance_profile again
    ret = await hub.states.aws.iam.instance_profile.absent(
        ctx,
        name=instance_profile_temp_name,
        resource_id=resource_id,
    )
    assert ret["result"], ret["comment"]
    assert not ret["old_state"] and not ret["new_state"]

    # Delete roles created by the test
    ret = await hub.states.aws.iam.role.absent(ctx, name=role_2)
    assert ret["result"], ret["comment"]
