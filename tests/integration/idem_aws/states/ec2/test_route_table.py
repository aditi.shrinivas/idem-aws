import copy
import os
import random
import time
import uuid

import pytest


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
async def test_route_table(
    hub, ctx, aws_ec2_vpc, aws_ec2_subnet, aws_ec2_nat_gateway, aws_ec2_internet_gateway
):
    # Create route table
    route_table_temp_name = "idem-test-route-table" + str(uuid.uuid4())
    vpc_id = aws_ec2_vpc.get("VpcId")
    tags = {"Name": route_table_temp_name}

    # Idem state --test
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await hub.states.aws.ec2.route_table.present(
        test_ctx,
        name=route_table_temp_name,
        vpc_id=vpc_id,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert vpc_id == resource.get("vpc_id")
    assert route_table_temp_name == resource.get("name")
    assert tags == resource.get("tags")

    # actual creation.
    ret = await hub.states.aws.ec2.route_table.present(
        ctx,
        name=route_table_temp_name,
        vpc_id=vpc_id,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert vpc_id == resource.get("vpc_id")
    assert tags == resource.get("tags")
    assert route_table_temp_name == resource.get("name")
    resource_id = resource.get("resource_id")

    # Test search route table
    filters = [
        {"name": "tag:Name", "values": [route_table_temp_name]},
        {"name": "vpc-id", "values": [vpc_id]},
    ]
    searched_ret = await hub.states.aws.ec2.route_table.search(
        ctx, name=route_table_temp_name, filters=filters
    )
    assert searched_ret["result"], searched_ret["comment"]
    assert searched_ret.get("old_state") and searched_ret.get("new_state")
    assert searched_ret.get("old_state") == searched_ret.get("new_state")

    searched_new_state = searched_ret.get("new_state")

    assert vpc_id == searched_new_state.get("vpc_id")
    assert tags == searched_new_state.get("tags")
    assert route_table_temp_name == searched_new_state.get("name")
    assert resource_id == searched_new_state.get("resource_id")

    # create second route table
    secondary_route_table_name = "idem-test-route-table" + str(uuid.uuid4())
    secondary_route_table_tags = {"Name": secondary_route_table_name}
    ret = await hub.states.aws.ec2.route_table.present(
        ctx,
        name=secondary_route_table_name,
        vpc_id=vpc_id,
        tags=secondary_route_table_tags,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    secondary_route_table = ret.get("new_state")
    assert vpc_id == secondary_route_table.get("vpc_id")
    assert secondary_route_table_tags == secondary_route_table.get("tags")
    assert secondary_route_table_name == secondary_route_table.get("name")
    secondary_route_table_resource_id = secondary_route_table.get("resource_id")

    # Test search when more than one route table is present for the given filters
    filters = [{"name": "vpc-id", "values": [vpc_id]}]
    searched_ret = await hub.states.aws.ec2.route_table.search(
        ctx, name=route_table_temp_name, filters=filters
    )
    assert searched_ret["result"], searched_ret["comment"]
    assert searched_ret.get("old_state") and searched_ret.get("new_state")
    assert searched_ret.get("old_state") == searched_ret.get("new_state")
    route_table_id = searched_ret.get("new_state").get("resource_id")
    assert (
        f"More than one aws.ec2.route_table resource was found. Use resource {route_table_id}"
        in searched_ret["comment"]
    )

    # Delete second route_table
    ret = await hub.states.aws.ec2.route_table.absent(
        ctx,
        name=secondary_route_table_name,
        resource_id=secondary_route_table_resource_id,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert (
        hub.tool.aws.comment_utils.delete_comment(
            resource_type="aws.ec2.route_table", name=secondary_route_table_name
        )[0]
        in ret["comment"]
    )

    # Test search route table not present
    filters = [
        {"name": "tag:Name", "values": ["dummy_name"]},
        {"name": "vpc-id", "values": [vpc_id]},
    ]
    searched_ret = await hub.states.aws.ec2.route_table.search(
        ctx, name=route_table_temp_name, filters=filters
    )
    assert not searched_ret["result"], searched_ret["comment"]
    assert (
        f"Unable to find aws.ec2.route_table '{route_table_temp_name}'"
        in searched_ret["comment"]
    )
    assert not searched_ret.get("old_state") and not searched_ret.get("new_state")

    new_routes = resource.get("routes")

    # Idem state --test with tags unchanged
    ret = await hub.states.aws.ec2.route_table.present(
        test_ctx,
        name=resource_id,
        resource_id=resource_id,
        vpc_id=vpc_id,
        tags=None,
        routes=None,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert new_routes == resource.get("routes")
    assert tags == resource.get("tags")

    # Removing all routes and tags unchanged
    ret = await hub.states.aws.ec2.route_table.present(
        ctx,
        name=resource_id,
        resource_id=resource_id,
        vpc_id=vpc_id,
        tags=None,
        routes=None,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert new_routes == resource.get("routes")
    assert tags == resource.get("tags")

    cidr_block = os.getenv("IT_TEST_EC2_ROUTE_TABLE_DESTINATION_CIDR_BLOCK")
    num: () = lambda: random.randint(0, 255)
    if cidr_block is None:
        cidr_block = f"172.31.{num()}.0/24"
    new_routes.append(
        {
            "DestinationCidrBlock": cidr_block,
            "NatGatewayId": aws_ec2_nat_gateway.get("resource_id"),
        }
    )

    # Test updating tags, routes
    new_tags = {"new-name": resource_id}

    # Idem state --test. Perform a dummy update.
    ret = await hub.states.aws.ec2.route_table.present(
        test_ctx,
        name=route_table_temp_name,
        resource_id=resource_id,
        vpc_id=vpc_id,
        tags=new_tags,
        routes=new_routes,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert new_tags == resource.get("tags")
    new_route_added = resource.get("routes")[1]
    assert (
        new_route_added["DestinationCidrBlock"] == new_routes[1]["DestinationCidrBlock"]
    )
    assert new_route_added["NatGatewayId"] == new_routes[1]["NatGatewayId"]

    for i in range(5):
        # we can associate NAT gateway to route table only if its in available state.
        ret = await hub.exec.boto3.client.ec2.describe_nat_gateways(
            ctx, NatGatewayIds=[aws_ec2_nat_gateway.get("resource_id")]
        )
        state = (
            ret["ret"]["NatGateways"][0]["State"]
            if ret["result"] and ret["ret"]["NatGateways"]
            else None
        )
        if state and state.casefold() == "pending":
            time.sleep(60)
        elif state and state.casefold() == "available":
            break

    # do an actual update.
    ret = await hub.states.aws.ec2.route_table.present(
        ctx,
        name=route_table_temp_name,
        resource_id=resource_id,
        vpc_id=vpc_id,
        tags=new_tags,
        routes=new_routes,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert new_tags == resource.get("tags")
    assert len(resource.get("routes")) == 2
    new_route_added = (
        resource.get("routes")[0]
        if (resource.get("routes")[1]).get("NatGatewayId") is None
        else resource.get("routes")[1]
    )
    assert (
        new_route_added["DestinationCidrBlock"] == new_routes[1]["DestinationCidrBlock"]
    )
    assert new_route_added["NatGatewayId"] == new_routes[1]["NatGatewayId"]

    # Idem state --test. Removing routes and tags unchanged
    new_routes = [new_routes[0]]
    ret = await hub.states.aws.ec2.route_table.present(
        test_ctx,
        name=route_table_temp_name,
        resource_id=resource_id,
        vpc_id=vpc_id,
        tags=None,
        routes=new_routes,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert new_routes == resource.get("routes")
    assert resource.get("tags") == new_tags

    # Removing routes and tags unchanged
    new_routes = [new_routes[0]]
    ret = await hub.states.aws.ec2.route_table.present(
        ctx,
        name=route_table_temp_name,
        resource_id=resource_id,
        vpc_id=vpc_id,
        tags=None,
        routes=new_routes,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert new_routes == resource.get("routes")
    assert resource.get("tags") == new_tags

    # Describe route_table
    describe_ret = await hub.states.aws.ec2.route_table.describe(ctx)
    assert resource_id in describe_ret
    hub.tool.utils.verify_in_list(
        describe_ret[resource_id]["aws.ec2.route_table.present"], "tags", new_tags
    )

    # Idem state --test. Delete route table.
    ret = await hub.states.aws.ec2.route_table.absent(
        test_ctx, name=route_table_temp_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert (
        f"Would delete aws.ec2.route_table '{route_table_temp_name}'" in ret["comment"]
    )

    # Delete route_table
    ret = await hub.states.aws.ec2.route_table.absent(
        ctx, name=route_table_temp_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert (
        hub.tool.aws.comment_utils.delete_comment(
            resource_type="aws.ec2.route_table", name=route_table_temp_name
        )[0]
        in ret["comment"]
    )

    # Trying to delete an already deleted resource. It should say resource already got deleted.
    ret = await hub.states.aws.ec2.route_table.absent(
        ctx, name=route_table_temp_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type="aws.ec2.route_table", name=route_table_temp_name
        )[0]
        in ret["comment"]
    )
    assert (not ret["old_state"]) and (not ret["new_state"])
