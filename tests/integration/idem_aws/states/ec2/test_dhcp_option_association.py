import copy

import pytest


@pytest.mark.asyncio
async def test_dhcp_option(hub, ctx, aws_ec2_vpc, aws_ec2_vpc_2, aws_ec2_dhcp_options):
    # Disassociating options from a VPC is failing with this following error:
    # 'ClientError: An error occurred (InvalidDhcpOptionID.NotFound) when calling the AssociateDhcpOptions operation:
    #  DhcpOptionID default does not exist.'
    # However, official documentation from AWS has specifically stated to associate 'default'
    # 'The ID of the DHCP options set, or default to associate no DHCP options with the VPC.'
    if hub.tool.utils.is_running_localstack(ctx):
        return

    # Associate a vpc id with dhcp options with test flag
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await hub.states.aws.ec2.dhcp_option_association.present(
        test_ctx,
        name=aws_ec2_dhcp_options.get("name"),
        vpc_id=aws_ec2_vpc.get("VpcId"),
        dhcp_id=aws_ec2_dhcp_options.get("resource_id"),
    )
    assert ret["result"], ret["comment"]
    assert (
        f"Would create aws.ec2.dhcp_option_association '{aws_ec2_dhcp_options.get('name')}'"
        in ret["comment"]
    )
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert resource.get("vpc_id") == aws_ec2_vpc.get("VpcId")
    assert resource.get("dhcp_id") == aws_ec2_dhcp_options.get("resource_id")

    # Associate a vpc_id with invalid DHCP Options ID.
    ret = await hub.states.aws.ec2.dhcp_option_association.present(
        ctx,
        name=aws_ec2_dhcp_options.get("name"),
        vpc_id=aws_ec2_vpc.get("VpcId"),
        dhcp_id="invalid-dhcp-id",
    )
    assert not ret["result"], ret["comment"]
    assert (
        f"Incorrect aws.ec2.dhcp_option_association id: 'invalid-dhcp-id'. Couldn't find DHCP Options with given id."
        in ret["comment"]
    )

    # Associate a vpc_id with DHCP options with no test flag.
    ret = await hub.states.aws.ec2.dhcp_option_association.present(
        ctx,
        name=aws_ec2_dhcp_options.get("name"),
        vpc_id=aws_ec2_vpc.get("VpcId"),
        dhcp_id=aws_ec2_dhcp_options.get("resource_id"),
    )
    assert ret["result"], ret["comment"]
    assert (
        f"Created aws.ec2.dhcp_option_association '{aws_ec2_dhcp_options.get('name')}'"
        in ret["comment"]
    )
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    resource_id = resource.get("resource_id")
    assert resource.get("vpc_id") == aws_ec2_vpc.get("VpcId")
    assert resource.get("dhcp_id") == aws_ec2_dhcp_options.get("resource_id")

    # test describe dhcp
    describe_ret = await hub.states.aws.ec2.dhcp_option_association.describe(ctx)
    assert describe_ret[resource_id]
    resource = describe_ret[resource_id]["aws.ec2.dhcp_option_association.present"][0]
    assert resource.get("name") == aws_ec2_dhcp_options.get("resource_id")
    assert resource.get("vpc_id") == aws_ec2_vpc.get("VpcId")

    # Associate another vpc_id with DHCP options. With test flag.
    ret = await hub.states.aws.ec2.dhcp_option_association.present(
        test_ctx,
        name=aws_ec2_dhcp_options.get("name"),
        vpc_id=aws_ec2_vpc_2.get("VpcId"),
        dhcp_id=aws_ec2_dhcp_options.get("resource_id"),
    )
    assert ret["result"], ret["comment"]
    assert (
        f"Would create aws.ec2.dhcp_option_association '{aws_ec2_dhcp_options.get('name')}'"
        in ret["comment"]
    )
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert resource.get("vpc_id") == aws_ec2_vpc_2.get("VpcId")
    assert resource.get("dhcp_id") == aws_ec2_dhcp_options.get("resource_id")

    # Associate another vpc_id with DHCP options.
    ret = await hub.states.aws.ec2.dhcp_option_association.present(
        ctx,
        name=aws_ec2_dhcp_options.get("name"),
        vpc_id=aws_ec2_vpc_2.get("VpcId"),
        dhcp_id=aws_ec2_dhcp_options.get("resource_id"),
    )
    assert ret["result"], ret["comment"]
    assert (
        f"Created aws.ec2.dhcp_option_association '{aws_ec2_dhcp_options.get('name')}'"
        in ret["comment"]
    )
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert resource.get("vpc_id") == aws_ec2_vpc_2.get("VpcId")
    assert resource.get("dhcp_id") == aws_ec2_dhcp_options.get("resource_id")

    # Delete DHCP-VPC association with test flag
    ret = await hub.states.aws.ec2.dhcp_option_association.absent(
        test_ctx,
        name=aws_ec2_dhcp_options.get("name"),
        vpc_id=aws_ec2_vpc_2.get("VpcId"),
        dhcp_id=aws_ec2_dhcp_options.get("resource_id"),
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and not ret["new_state"]
    assert (
        f"Would delete aws.ec2.dhcp_option_association '{aws_ec2_dhcp_options.get('name')}'"
        in ret["comment"]
    )
    resource = ret.get("old_state")
    assert resource.get("vpc_id") == aws_ec2_vpc_2.get("VpcId")
    assert resource.get("dhcp_id") == aws_ec2_dhcp_options.get("resource_id")

    # Delete with invalid dhcp_options_id
    ret = await hub.states.aws.ec2.dhcp_option_association.absent(
        ctx,
        name=aws_ec2_dhcp_options.get("name"),
        vpc_id=aws_ec2_vpc_2.get("VpcId"),
        dhcp_id="invalid-xyz-abc",
    )
    assert not ret["result"], ret["comment"]
    assert (
        f"Incorrect aws.ec2.dhcp_option_association id: 'invalid-xyz-abc'. Couldn't find DHCP Options with given id."
        in ret["comment"]
    )

    # Delete with invalid vpc_id.
    ret = await hub.states.aws.ec2.dhcp_option_association.absent(
        ctx,
        name=aws_ec2_dhcp_options.get("name"),
        vpc_id="invalid-xyz-abc",
        dhcp_id=aws_ec2_dhcp_options.get("resource_id"),
    )
    assert ret["result"], ret["comment"]
    assert (
        f"aws.ec2.dhcp_option_association '{aws_ec2_dhcp_options.get('name')}' "
        f"for vpc_id 'invalid-xyz-abc' is already absent!" in ret["comment"]
    )

    # Delete DHCP-VPC association.
    ret = await hub.states.aws.ec2.dhcp_option_association.absent(
        ctx,
        name=aws_ec2_dhcp_options.get("name"),
        vpc_id=aws_ec2_vpc_2.get("VpcId"),
        dhcp_id=aws_ec2_dhcp_options.get("resource_id"),
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and not ret["new_state"]
    assert (
        f"Deleted aws.ec2.dhcp_option_association '{aws_ec2_dhcp_options.get('name')}'"
        in ret["comment"]
    )
    resource = ret.get("old_state")
    assert resource.get("vpc_id") == aws_ec2_vpc_2.get("VpcId")
    assert resource.get("dhcp_id") == aws_ec2_dhcp_options.get("resource_id")

    # Delete another DHCP-VPC association.
    ret = await hub.states.aws.ec2.dhcp_option_association.absent(
        ctx,
        name=aws_ec2_dhcp_options.get("name"),
        vpc_id=aws_ec2_vpc.get("VpcId"),
        dhcp_id=aws_ec2_dhcp_options.get("resource_id"),
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and not ret["new_state"]
    assert (
        f"Deleted aws.ec2.dhcp_option_association '{aws_ec2_dhcp_options.get('name')}'"
        in ret["comment"]
    )
    resource = ret.get("old_state")
    assert resource.get("vpc_id") == aws_ec2_vpc.get("VpcId")
    assert resource.get("dhcp_id") == aws_ec2_dhcp_options.get("resource_id")

    # Deleting same DHCP-VPC association shouldn't result in error.
    ret = await hub.states.aws.ec2.dhcp_option_association.absent(
        ctx,
        name=aws_ec2_dhcp_options.get("name"),
        vpc_id=aws_ec2_vpc.get("VpcId"),
        dhcp_id=aws_ec2_dhcp_options.get("resource_id"),
    )
    assert ret["result"], ret["comment"]
    assert (
        f"aws.ec2.dhcp_option_association '{aws_ec2_dhcp_options.get('name')}' for vpc_id '{aws_ec2_vpc.get('VpcId')}' "
        f"is already absent!" in ret["comment"]
    )
