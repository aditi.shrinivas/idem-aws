import copy
import os
import random
import time
import uuid
from collections import ChainMap

import pytest


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
async def test_vpc(hub, ctx):
    vpc_temp_name = "idem-test-vpc-" + str(uuid.uuid4())
    cidr_block = os.getenv("IT_TEST_EC2_VPC_CIDR_BLOCK")
    num: () = lambda: random.randint(0, 255)
    if cidr_block is None:
        cidr_block = f"192.168.{num()}.0/24"
    cidr_block_association_set = [{"CidrBlock": cidr_block}]
    tags = {"Name": vpc_temp_name}

    # Create vpc with test flag
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await hub.states.aws.ec2.vpc.present(
        test_ctx,
        name=vpc_temp_name,
        cidr_block_association_set=copy.deepcopy(cidr_block_association_set),
        instance_tenancy="default",
        tags=tags,
        enable_dns_hostnames=True,
        enable_dns_support=True,
    )
    assert ret["result"], ret["comment"]
    assert f"Would create aws.ec2.vpc '{vpc_temp_name}'" in ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert cidr_block == resource.get("cidr_block_association_set")[0].get("CidrBlock")
    assert tags == resource.get("tags")
    assert "default" == resource.get("instance_tenancy")
    assert vpc_temp_name == resource.get("name")
    assert resource.get("enable_dns_hostnames")
    assert resource.get("enable_dns_support")

    # Create vpc
    ret = await hub.states.aws.ec2.vpc.present(
        ctx,
        name=vpc_temp_name,
        cidr_block_association_set=copy.deepcopy(cidr_block_association_set),
        instance_tenancy="default",
        tags=tags,
        enable_dns_hostnames=True,
        enable_dns_support=True,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert cidr_block == resource.get("cidr_block_association_set")[0].get("CidrBlock")
    assert tags == resource.get("tags")
    assert "default" == resource.get("instance_tenancy")
    assert vpc_temp_name == resource.get("name")
    assert resource.get("enable_dns_hostnames")
    assert resource.get("enable_dns_support")
    resource_id = resource.get("resource_id")

    # Describe vpc
    describe_ret = await hub.states.aws.ec2.vpc.describe(ctx)
    assert resource_id in describe_ret
    # Verify that the describe output format is correct
    assert "aws.ec2.vpc.present" in describe_ret.get(resource_id)
    described_resource = describe_ret.get(resource_id).get("aws.ec2.vpc.present")
    described_resource_map = dict(ChainMap(*described_resource))
    assert "cidr_block_association_set" in described_resource_map
    assert cidr_block == described_resource_map["cidr_block_association_set"][0].get(
        "CidrBlock"
    )
    assert "default" == described_resource_map.get("instance_tenancy")
    assert described_resource_map.get("tags") == tags
    assert described_resource_map.get("enable_dns_support")
    assert described_resource_map.get("enable_dns_hostnames")

    # TODO: This part to be removed after deprecating search state
    # Test search vpc
    filters = [{"name": "tag-key", "values": ["Name"]}]
    search_ret = await hub.states.aws.ec2.vpc.search(
        ctx, name=f"{vpc_temp_name}-search", filters=filters, resource_id=resource_id
    )
    assert search_ret["result"], search_ret["comment"]
    assert search_ret.get("old_state") and search_ret.get("new_state")
    resource = search_ret.get("new_state")
    assert cidr_block == resource.get("cidr_block_association_set")[0].get("CidrBlock")
    assert tags == resource.get("tags")
    assert "default" == resource.get("instance_tenancy")
    assert f"{vpc_temp_name}-search" == resource.get("name")
    assert resource.get("enable_dns_hostnames")
    assert resource.get("enable_dns_support")

    # Test search vpc not present
    filters = [{"name": "tag-key", "values": ["Not-present"]}]
    search_ret = await hub.states.aws.ec2.vpc.search(
        ctx, name=f"{vpc_temp_name}-search", filters=filters
    )
    assert search_ret["result"], search_ret["comment"]
    assert (
        hub.tool.aws.comment_utils.get_empty_comment(
            resource_type="aws.ec2.vpc", name=f"{vpc_temp_name}-search"
        )
        in search_ret["comment"]
    )
    assert not search_ret.get("old_state") and not search_ret.get("new_state")

    # Test associating cidr block and adding tags
    cidr_block_2 = os.getenv("IT_TEST_EC2_VPC_CIDR_BLOCK_2")
    if cidr_block_2 is None:
        cidr_block_2 = f"192.168.{num()}.0/24"
    cidr_block_association_set.append({"CidrBlock": cidr_block_2})
    tags.update(
        {
            f"idem-test-vpc-key-{str(int(time.time()))}": f"idem-test-vpc-value-{str(int(time.time()))}",
        }
    )
    # Update vpc with test flag
    ret = await hub.states.aws.ec2.vpc.present(
        test_ctx,
        name=vpc_temp_name,
        resource_id=resource_id,
        cidr_block_association_set=copy.deepcopy(cidr_block_association_set),
        tags=tags,
        enable_dns_hostnames=False,
        enable_dns_support=False,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    real_cidr_blocks = [
        cidr.get("CidrBlock") for cidr in resource.get("cidr_block_association_set")
    ]
    assert 2 == len(real_cidr_blocks)
    assert cidr_block in real_cidr_blocks
    assert cidr_block_2 in real_cidr_blocks
    assert tags == resource.get("tags")
    assert resource.get("enable_dns_hostnames") is True
    assert resource.get("enable_dns_support") is False

    # Update vpc in real
    ret = await hub.states.aws.ec2.vpc.present(
        ctx,
        name=vpc_temp_name,
        resource_id=resource_id,
        cidr_block_association_set=copy.deepcopy(cidr_block_association_set),
        tags=tags,
        enable_dns_hostnames=False,
        enable_dns_support=False,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    real_cidr_blocks = [
        cidr.get("CidrBlock") for cidr in resource.get("cidr_block_association_set")
    ]
    assert 2 == len(real_cidr_blocks)
    assert cidr_block in real_cidr_blocks
    assert cidr_block_2 in real_cidr_blocks
    real_vpc_tags = resource.get("tags")
    assert real_vpc_tags
    assert real_vpc_tags == tags
    assert resource.get("enable_dns_hostnames") is True
    assert resource.get("enable_dns_support") is False

    # Test disassociating cidr and deleting tags
    tags.pop("Name")
    ret = await hub.states.aws.ec2.vpc.present(
        ctx,
        name=vpc_temp_name,
        resource_id=resource_id,
        cidr_block_association_set=copy.deepcopy([cidr_block_association_set[0]]),
        tags=tags,
        enable_dns_hostnames=True,
        enable_dns_support=True,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert 1 == len(resource.get("cidr_block_association_set"))
    assert cidr_block_association_set[0].get("CidrBlock") == resource.get(
        "cidr_block_association_set"
    )[0].get("CidrBlock")
    assert tags == resource.get("tags")
    assert resource.get("enable_dns_hostnames")
    assert resource.get("enable_dns_support")

    # Delete vpc with test flag
    ret = await hub.states.aws.ec2.vpc.absent(
        test_ctx, name=vpc_temp_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert f"Would delete aws.ec2.vpc '{vpc_temp_name}'" in ret["comment"]

    # Delete vpc
    ret = await hub.states.aws.ec2.vpc.absent(
        ctx, name=vpc_temp_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")

    # Deleting vpc again should be an no-op
    ret = await hub.states.aws.ec2.vpc.absent(
        ctx, name=vpc_temp_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert f"aws.ec2.vpc '{vpc_temp_name}' already absent" in ret["comment"]


@pytest.mark.asyncio
async def test_vpc_absent_with_none_resource_id(hub, ctx):
    vpc_temp_name = "idem-test-vpc-" + str(uuid.uuid4())
    # Delete vpc with resource_id as None. Result in no-op.
    ret = await hub.states.aws.ec2.vpc.absent(ctx, name=vpc_temp_name, resource_id=None)
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert f"aws.ec2.vpc '{vpc_temp_name}' already absent" in ret["comment"]
