import pytest
import yaml
from pytest_idem.runner import idem_cli
from pytest_idem.runner import run_yaml_block

# Parametrization options for running each test with --test first and then without --test
PARAMETRIZE = dict(argnames="__test", argvalues=[True, False], ids=["--test", "run"])

INSTANCE_TYPE = """
small:
  aws.ec2.instance_type.search:
    - filters:
      - name: instance-type
        values:
          - '*.small'
      - name: hypervisor
        values:
          - xen
      - name: processor-info.supported-architecture
        values:
          - x86_64
"""

SEARCH_STATE = """
verify:
  aws.ec2.spot_instance_request.search:
    - resource_id: ${aws.ec2.spot_instance_request:test_resource:resource_id}
"""

IMAGE_ANY = """
test_ami:
  aws.ec2.ami.search:
    - most_recent: False
    - filters:
      - name: image-type
        values:
          - machine
      - name: state
        values:
          - available
      - name: hypervisor
        values:
          - xen
      - name: architecture
        values:
          - x86_64
"""


PRESENT_STATE = f"""
{IMAGE_ANY}
{INSTANCE_TYPE}
{SEARCH_STATE}
test_resource:
  aws.ec2.spot_instance_request.present:
    - launch_specification:
        ImageId: ${{aws.ec2.ami:test_ami:resource_id}}
        InstanceType: ${{aws.ec2.instance_type:small:resource_id}}
"""


ABSENT_STATE = f"""
test_resource:
  aws.ec2.spot_instance_request.absent

{SEARCH_STATE}
"""


@pytest.mark.localstack(False)
@pytest.mark.parametrize(**PARAMETRIZE)
def test_starting_absent(esm_cache, acct_data, __test):
    """
    Verify that the absent state is successful for a resource that never existed
    """
    # Run the state defined in the yaml block
    ret = run_yaml_block(
        ABSENT_STATE, managed_state=esm_cache, acct_data=acct_data, test=__test
    )
    instance_ret = ret[
        "aws.ec2.spot_instance_request_|-test_resource_|-test_resource_|-absent"
    ]
    assert instance_ret["result"], instance_ret["comment"]

    # Verify that the search state is not able to find the resource
    search_ret = ret["aws.ec2.spot_instance_request_|-verify_|-verify_|-search"]
    assert not search_ret["result"], search_ret["comment"]

    assert instance_ret["new_state"] is None
    assert "aws.ec2.instance_|-test_resource_|-test_resource_|-" not in esm_cache


@pytest.mark.localstack(False)
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="present")
def test_present(esm_cache, acct_data, __test):
    """
    Create a brand new instance based on an image that exists locally
    """
    # Run the state defined in the yaml block
    ret = run_yaml_block(
        PRESENT_STATE, managed_state=esm_cache, acct_data=acct_data, test=__test
    )
    instance_ret = ret[
        "aws.ec2.spot_instance_request_|-test_resource_|-test_resource_|-present"
    ]
    assert instance_ret["result"], instance_ret["comment"]

    # Verify that the search state is able to find the resource
    search_ret = ret["aws.ec2.spot_instance_request_|-verify_|-verify_|-search"]

    # On the first run with --test, the search will fail
    if __test:
        assert not search_ret["result"], search_ret["comment"]
    else:
        assert search_ret["result"], search_ret["comment"]
        # Verify that the search state returns the exact same data as the present state
        assert (
            search_ret["new_state"]
            == instance_ret["new_state"]
            == esm_cache[
                "aws.ec2.spot_instance_request_|-test_resource_|-test_resource_|-"
            ]
        )


@pytest.mark.localstack(False)
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(depends=["present"])
def test_already_present(esm_cache, acct_data, __test):
    """
    The instance should exist in the cache, verify that the resource id gets populated and no changes are made
    """
    # Verify that the cache exists now
    assert (
        esm_cache
    ), "Cache does not exist! It should exist now after the previous test"

    # Run the state defined in the yaml block
    ret = run_yaml_block(
        PRESENT_STATE, managed_state=esm_cache, acct_data=acct_data, test=__test
    )
    instance_ret = ret[
        "aws.ec2.spot_instance_request_|-test_resource_|-test_resource_|-present"
    ]
    assert instance_ret["result"], instance_ret["comment"]

    if __test:
        return

    assert not instance_ret["changes"], instance_ret["changes"]

    # Verify that the search state is able to find the resource
    search_ret = ret["aws.ec2.spot_instance_request_|-verify_|-verify_|-search"]
    assert search_ret["result"], search_ret["comment"]
    # Verify that the search state returns the exact same data as the present state
    assert (
        search_ret["new_state"]
        == instance_ret["new_state"]
        == esm_cache["aws.ec2.spot_instance_request_|-test_resource_|-test_resource_|-"]
    )


# This only works on real aws, not localstack
@pytest.mark.localstack(False)
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(depends=["present"])
def test_describe(esm_cache, acct_data, __test):
    """
    Describe all instances and run the "present" state the described instance created for this module.
    No changes should be made and present/search/describe should have equivalent parameters.
    """
    # Get the resource_id of the spot_instance_request created by this module
    resource_id = esm_cache[
        "aws.ec2.spot_instance_request_|-test_resource_|-test_resource_|-"
    ]["resource_id"]

    # Describe all instances
    ret = idem_cli("describe", "aws.ec2.spot_instance_request", acct_data=acct_data)
    return

    assert resource_id in ret.keys()  # noqa

    # Get the state for our new instance, that was created by describe
    single_described_state = yaml.safe_dump({"described": ret[resource_id]})

    # Run the present state for our resource created by describe
    ret = run_yaml_block(
        single_described_state,
        managed_state=esm_cache,
        acct_data=acct_data,
        test=__test,
    )
    tag = f"aws.ec2.spot_instance_request_|-described_|-{resource_id}_|-present"
    assert tag in ret.keys()
    instance_ret = ret[tag]
    assert instance_ret["result"], instance_ret["comment"]

    # No changes should have been made!
    # We just created this state from describe
    assert instance_ret["new_state"] == instance_ret["old_state"]
    assert instance_ret["changes"].get("new") == instance_ret["changes"].get("old")
    assert not instance_ret["changes"], instance_ret["changes"]
    assert (
        instance_ret["new_state"]
        == esm_cache["aws.ec2.spot_instance_request_|-test_resource_|-test_resource_|-"]
    )


# TODO make a change to the resource and verify the change

# Cleanup time


@pytest.mark.localstack(False)
@pytest.mark.dependency(depends=["present"])
def test_clean_spot_instances(esm_cache, acct_data):
    """
    Verify that any spot instances created by the spot instance request are terminated
    """
    # Remove based on spot instance id
    STATE = f"""
    instance:
      aws.ec2.instance.search:
        - filters:
          - name: spot-instance-request-id
            values:
              - {esm_cache['aws.ec2.spot_instance_request_|-test_resource_|-test_resource_|-']['resource_id']}
    remove_instance:
      aws.ec2.instance.absent:
        - resource_id: ${{aws.ec2.instance:instance:resource_id}}
    """
    run_yaml_block(STATE, acct_data=acct_data)


@pytest.mark.localstack(False)
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(depends=["present"])
def test_absent(esm_cache, acct_data, __test):
    """
    Terminate the instance
    """
    assert (
        esm_cache
    ), "Cache does not exist! It should exist now after the previous test"

    # Run the state defined in the yaml block
    ret = run_yaml_block(
        ABSENT_STATE, managed_state=esm_cache, acct_data=acct_data, test=__test
    )
    instance_ret = ret[
        "aws.ec2.spot_instance_request_|-test_resource_|-test_resource_|-absent"
    ]
    assert instance_ret["result"], instance_ret["comment"]

    # Verify that the search state is not able to find the resource
    search_ret = ret["aws.ec2.spot_instance_request_|-verify_|-verify_|-search"]
    assert not search_ret["result"], search_ret["comment"]

    assert instance_ret["new_state"] is None
    if __test:
        # It shouldn't have been removed with --test
        assert (
            "aws.ec2.spot_instance_request_|-test_resource_|-test_resource_|-"
            in esm_cache
        )
    else:
        # It should be removed now
        assert (
            "aws.ec2.spot_instance_request_|-test_resource_|-test_resource_|-"
            not in esm_cache
        )


@pytest.mark.localstack(False)
@pytest.mark.parametrize(**PARAMETRIZE)
def test_already_absent(esm_cache, acct_data, __test):
    """
    Verify that termination is idempotent
    """
    # Run the state defined in the yaml block
    ret = run_yaml_block(
        ABSENT_STATE, managed_state=esm_cache, acct_data=acct_data, test=__test
    )
    instance_ret = ret[
        "aws.ec2.spot_instance_request_|-test_resource_|-test_resource_|-absent"
    ]
    assert instance_ret["result"], instance_ret["comment"]

    # Verify that the search state is not able to find the resource
    search_ret = ret["aws.ec2.spot_instance_request_|-verify_|-verify_|-search"]
    assert not search_ret["result"], search_ret["comment"]

    assert instance_ret["new_state"] is None
    assert (
        "aws.ec2.spot_instance_request_|-test_resource_|-test_resource_|-"
        not in esm_cache
    )
