import copy
import time
import uuid
from collections import ChainMap

import pytest


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
async def test_subnet(hub, ctx, aws_ec2_vpc):
    ipv4_cidr_block = hub.tool.utils.get_sub_cidr_block(
        aws_ec2_vpc.get("CidrBlock"), 28
    )
    subnet_temp_name = "idem-test-subnet-" + str(int(time.time()))
    tags = {"Name": subnet_temp_name}
    map_public_ip_on_launch = True
    private_dns_name_options_on_launch = {
        "EnableResourceNameDnsAAAARecord": False,
        "EnableResourceNameDnsARecord": False,
        "HostnameType": "resource-name",
    }

    # Create subnet with test flag
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await hub.states.aws.ec2.subnet.present(
        test_ctx,
        name=subnet_temp_name,
        cidr_block=ipv4_cidr_block,
        vpc_id=aws_ec2_vpc.get("VpcId"),
        map_public_ip_on_launch=map_public_ip_on_launch,
        private_dns_name_options_on_launch=private_dns_name_options_on_launch,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert f"Would create aws.ec2.subnet '{subnet_temp_name}'" in ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert subnet_temp_name == resource.get("name")
    assert ipv4_cidr_block == resource.get("cidr_block")
    assert aws_ec2_vpc.get("VpcId") == resource.get("vpc_id")
    assert tags == resource.get("tags")
    assert map_public_ip_on_launch == resource.get("map_public_ip_on_launch")
    assert private_dns_name_options_on_launch == resource.get(
        "private_dns_name_options_on_launch"
    )

    # Create subnet
    ret = await hub.states.aws.ec2.subnet.present(
        ctx,
        name=subnet_temp_name,
        cidr_block=ipv4_cidr_block,
        vpc_id=aws_ec2_vpc.get("VpcId"),
        tags=tags,
        map_public_ip_on_launch=map_public_ip_on_launch,
        private_dns_name_options_on_launch=private_dns_name_options_on_launch,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert subnet_temp_name == resource.get("name")
    assert ipv4_cidr_block == resource.get("cidr_block")
    assert aws_ec2_vpc.get("VpcId") == resource.get("vpc_id")
    assert tags == resource.get("tags")
    resource_id = resource.get("resource_id")
    assert map_public_ip_on_launch == resource.get("map_public_ip_on_launch")
    if not hub.tool.utils.is_running_localstack(ctx):
        assert private_dns_name_options_on_launch == resource.get(
            "private_dns_name_options_on_launch"
        )

    # Test associating ipv6 cidr block and adding tags
    ipv6_cidr_block = None
    assign_ipv6_address_on_creation = None
    if hub.tool.utils.is_running_localstack(ctx):
        # Only try to associate ipv6 block with localstack, since the vpc on real aws does not have an ipv6 block
        ipv6_cidr_block = "2a05:d01c:74f:7200::/64"
        assign_ipv6_address_on_creation = True
    tags.update(
        {
            f"idem-test-subnet-key-{str(uuid.uuid4())}": f"idem-test-subnet-value-{str(uuid.uuid4())}"
        }
    )
    map_public_ip_on_launch = False
    private_dns_name_options_on_launch = {
        "EnableResourceNameDnsAAAARecord": False,
        "EnableResourceNameDnsARecord": False,
        "HostnameType": "resource-name",
    }

    # Update subnet with test flag
    ret = await hub.states.aws.ec2.subnet.present(
        test_ctx,
        name=subnet_temp_name,
        resource_id=resource_id,
        cidr_block=ipv4_cidr_block,
        vpc_id=aws_ec2_vpc.get("VpcId"),
        map_public_ip_on_launch=map_public_ip_on_launch,
        tags=tags,
        ipv6_cidr_block=ipv6_cidr_block,
        private_dns_name_options_on_launch=private_dns_name_options_on_launch,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    if hub.tool.utils.is_running_localstack(ctx):
        assert ipv6_cidr_block == resource.get("ipv6_cidr_block")
    assert tags == resource.get("tags")
    assert map_public_ip_on_launch == resource.get("map_public_ip_on_launch")
    if not hub.tool.utils.is_running_localstack(ctx):
        assert private_dns_name_options_on_launch == resource.get(
            "private_dns_name_options_on_launch"
        )

    # Update subnet in real
    ret = await hub.states.aws.ec2.subnet.present(
        ctx,
        name=subnet_temp_name,
        resource_id=resource_id,
        cidr_block=ipv4_cidr_block,
        vpc_id=aws_ec2_vpc.get("VpcId"),
        map_public_ip_on_launch=map_public_ip_on_launch,
        tags=tags,
        ipv6_cidr_block=ipv6_cidr_block,
        assign_ipv6_address_on_creation=assign_ipv6_address_on_creation,
        private_dns_name_options_on_launch=private_dns_name_options_on_launch,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    if hub.tool.utils.is_running_localstack(ctx):
        assert ipv6_cidr_block == resource.get("ipv6_cidr_block")
        assert assign_ipv6_address_on_creation == resource.get(
            "assign_ipv6_address_on_creation"
        )
    else:
        assert private_dns_name_options_on_launch == resource.get(
            "private_dns_name_options_on_launch"
        )
    assert tags == resource.get("tags")
    assert map_public_ip_on_launch == resource.get("map_public_ip_on_launch")

    # Test deleting tags
    tags = {"Name": subnet_temp_name}
    ret = await hub.states.aws.ec2.subnet.present(
        ctx,
        name=subnet_temp_name,
        resource_id=resource_id,
        cidr_block=ipv4_cidr_block,
        vpc_id=aws_ec2_vpc.get("VpcId"),
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    if hub.tool.utils.is_running_localstack(ctx):
        # Make sure "Ipv6CidrBlockAssociationSet" remains the same
        assert ipv6_cidr_block == resource.get("ipv6_cidr_block")
    assert tags == resource.get("tags")

    # Describe subnet
    describe_ret = await hub.states.aws.ec2.subnet.describe(ctx)
    assert resource_id in describe_ret
    # Verify that the describe output format is correct
    assert "aws.ec2.subnet.present" in describe_ret.get(resource_id)
    described_resource = describe_ret.get(resource_id).get("aws.ec2.subnet.present")
    described_resource_map = dict(ChainMap(*described_resource))
    assert aws_ec2_vpc.get("VpcId") == described_resource_map.get("vpc_id")
    assert ipv4_cidr_block == described_resource_map.get("cidr_block")
    assert tags == described_resource_map.get("tags")
    assert map_public_ip_on_launch == described_resource_map.get(
        "map_public_ip_on_launch"
    )

    if hub.tool.utils.is_running_localstack(ctx):
        assert ipv6_cidr_block == described_resource_map.get("ipv6_cidr_block")
        assert assign_ipv6_address_on_creation == described_resource_map.get(
            "assign_ipv6_address_on_creation"
        )
    else:
        assert private_dns_name_options_on_launch == resource.get(
            "private_dns_name_options_on_launch"
        )

    # TODO: This part of testing should be removed when we deprecate out search()
    # Search for subnet present in the region
    search_ret = await hub.states.aws.ec2.subnet.search(
        ctx,
        name=f"{subnet_temp_name}-search",
        cidr_block=ipv4_cidr_block,
        default_for_az="false",
        tags=tags,
    )
    assert search_ret["result"], search_ret["comment"]
    assert search_ret.get("old_state") and search_ret.get("new_state")
    resource = search_ret.get("new_state")
    assert f"{subnet_temp_name}-search" == resource.get("name")
    assert ipv4_cidr_block == resource.get("cidr_block")
    if hub.tool.utils.is_running_localstack(ctx):
        assert ipv6_cidr_block == resource.get("ipv6_cidr_block")
        assert assign_ipv6_address_on_creation == described_resource_map.get(
            "assign_ipv6_address_on_creation"
        )
    assert aws_ec2_vpc.get("VpcId") == resource.get("vpc_id")
    assert tags == described_resource_map.get("tags")

    # Delete vpc with test flag
    ret = await hub.states.aws.ec2.subnet.absent(
        test_ctx, name=subnet_temp_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert f"Would delete aws.ec2.subnet '{subnet_temp_name}'" in ret["comment"]

    # Delete subnet
    ret = await hub.states.aws.ec2.subnet.absent(
        ctx, name=subnet_temp_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")

    # Delete subnet again
    ret = await hub.states.aws.ec2.subnet.absent(
        ctx, name=subnet_temp_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert f"aws.ec2.subnet '{subnet_temp_name}' already absent" in ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])


@pytest.mark.asyncio
async def test_subnet_absent_with_none_resource_id(hub, ctx):
    subnet_temp_name = "idem-test-subnet-" + str(uuid.uuid4())
    # Delete subnet with resource_id as None. Result in no-op.
    ret = await hub.states.aws.ec2.subnet.absent(
        ctx, name=subnet_temp_name, resource_id=None
    )
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert f"aws.ec2.subnet '{subnet_temp_name}' already absent" in ret["comment"]
