import time

import pytest


@pytest.mark.asyncio
async def test_caller_identity(hub, ctx):
    caller_identity_name = "idem-test-caller-identity-" + str(int(time.time()))
    # Test search caller_identity
    search_ret = await hub.states.aws.caller_identity.search(
        ctx, name=caller_identity_name
    )
    assert search_ret["result"], search_ret["comment"]
    assert search_ret["old_state"] and search_ret["new_state"]
    resource = search_ret["new_state"]
    assert caller_identity_name == resource["name"]
    assert resource["user_id"]
    assert resource["account_id"]
    assert resource["arn"]
