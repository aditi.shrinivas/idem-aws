import dict_tools.data


async def test_caller_identity(hub, ctx):
    # Test get caller_identity
    ret = await hub.exec.aws.sts.caller_identity.get_caller_identity(ctx)

    assert ret.result, ret.comment
    # Verify that all the values were properly retrieved for the named session
    assert ret.ret["UserId"]
    assert ret.ret["Account"]
    assert ret.ret["Arn"]


async def test_caller_identity_failed(hub, ctx):
    # Test get caller_identity fail
    dummyProfile = {}
    dummyProfile["aws_access_key_id"] = "DummyKey"
    dummyProfile["aws_secret_access_key"] = "DummySecret"
    dummyProfile["aws_session_token"] = "DummySession"
    ctx = dict_tools.data.NamespaceDict(acct=dummyProfile)
    ret = await hub.exec.aws.sts.caller_identity.get_caller_identity(ctx)
    assert not ret.result, ret.comment
    assert (
        "ClientError: An error occurred (InvalidClientTokenId) when calling the GetCallerIdentity operation: The security token included in the request is invalid."
        in ret.comment[0]
    )
