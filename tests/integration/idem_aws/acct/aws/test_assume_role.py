import pytest
from pytest_idem.runner import run_yaml_block

# Cleanup the roles created for this test
ROLE_ABSENT_STATE = """
test_assume_role:
  aws.iam.role.absent:
    - resource_id: test_assume_role
test_use_new_profile:
  aws.iam.role.absent:
    - resource_id: test_use_new_profile
"""

# Create a new role
NEW_ROLE_STATE = """
test_assume_role:
  aws.iam.role.present:
    # - test: False
    - acct_profile: source
    - assume_role_policy_document:
         Statement:
           - Action: "sts:AssumeRole"
             Effect: Deny
             Principal:
               Service: eks.amazonaws.com
         Version: "2012-10-17"
"""

# Assume the newly created role in a new acct profile
ASSUME_ROLE_STATE = """
assume_role:
  acct.profile:
    # - test: False
    - provider_name: aws
    - source_profile: source
    - assume_role:
        role_arn: ${aws.iam.role:test_assume_role:arn}
        role_session_name: 'test_role'
"""

# Fail to assume a role
ASSUME_ROLE_FAIL_STATE = """
assume_role:
  acct.profile:
    - provider_name: aws
    - source_profile: source
    - assume_role: {}
"""

# Use the new profile that assumes a new role in another state
USE_NEW_PROFILE_STATE = """
test_use_new_profile:
  aws.iam.role.present:
    - acct_profile: assume_role
    - require:
        - acct: assume_role
    - assume_role_policy_document:
         Statement:
           - Action: "sts:AssumeRole"
             Effect: Deny
             Principal:
               Service: eks.amazonaws.com
         Version: "2012-10-17"
"""


@pytest.fixture(name="acct_data")
def acct_data_(hub, ctx):
    return {"profiles": {"aws": {"default": ctx.acct, "source": ctx.acct}}}


@pytest.fixture(autouse=True, scope="function")
@pytest.mark.localstack(Pro=True)
def clean_role(hub, acct_data):
    """
    Cleanup the roles used in this test before and after each test
    """
    # Cleanup before
    ret = run_yaml_block(ROLE_ABSENT_STATE, acct_data=acct_data)
    for state_ret in ret.values():
        assert state_ret["result"], state_ret["comment"]

    yield

    # Cleanup after
    run_yaml_block(ROLE_ABSENT_STATE, acct_data=acct_data)
    for state_ret in ret.values():
        assert state_ret["result"], state_ret["comment"]


@pytest.mark.localstack(Pro=True)
def test_create_and_assume_role(hub, acct_data):
    """
    verify that we can create a new role and assume it for a subsequent state
    """
    state = f"{NEW_ROLE_STATE}\n{ASSUME_ROLE_STATE}\n{USE_NEW_PROFILE_STATE}"

    ret = run_yaml_block(state, acct_data=acct_data)

    test_assume_role_ret = ret[
        "aws.iam.role_|-test_assume_role_|-test_assume_role_|-present"
    ]
    assert test_assume_role_ret["result"] is True, test_assume_role_ret["comment"]

    assume_role_ret = ret["acct_|-assume_role_|-assume_role_|-profile"]
    assert assume_role_ret["result"] is True, assume_role_ret["comment"]

    test_use_new_profile_ret = ret[
        "aws.iam.role_|-test_use_new_profile_|-test_use_new_profile_|-present"
    ]
    assert test_use_new_profile_ret["result"] is True, test_use_new_profile_ret[
        "comment"
    ]


@pytest.mark.localstack(Pro=True)
def test_assume_role_fail(hub, acct_data):
    """
    verify that if assuming a role fails, that states that depend on that profile also fail
    """
    state = f"{ASSUME_ROLE_FAIL_STATE}\n{USE_NEW_PROFILE_STATE}"

    ret = run_yaml_block(state, acct_data=acct_data)

    test_use_new_profile_ret = ret[
        "aws.iam.role_|-test_use_new_profile_|-test_use_new_profile_|-present"
    ]
    assert (
        test_use_new_profile_ret["result"] is False
    ), f"State was successful when it should have failed: {test_use_new_profile_ret['comment']}"


@pytest.mark.skipif(
    True,
    reason="Creating a role then assuming it in a new profile is incompatible with --test",
)
@pytest.mark.localstack(Pro=True)
def test_create_and_assume_role_ctx_test(hub, acct_data):
    """
    verify that roles can be assumed when idem is run with --test
    """

    state = f"{NEW_ROLE_STATE}\n{ASSUME_ROLE_STATE}\n{USE_NEW_PROFILE_STATE}"

    ret = run_yaml_block(state, acct_data=acct_data, test=True)

    test_assume_role_ret = ret[
        "aws.iam.role_|-test_assume_role_|-test_assume_role_|-present"
    ]
    assert test_assume_role_ret["result"] is True, test_assume_role_ret["comment"]

    assume_role_ret = ret["acct_|-assume_role_|-assume_role_|-profile"]
    assert assume_role_ret["result"] is True, assume_role_ret["comment"]

    test_use_new_profile_ret = ret[
        "aws.iam.role_|-test_use_new_profile_|-test_use_new_profile_|-present"
    ]
    assert test_use_new_profile_ret["result"] is True, test_use_new_profile_ret[
        "comment"
    ]


@pytest.mark.localstack(Pro=True)
def test_create_and_assume_role_ctx_test_existing(hub, acct_data):
    """
    verify that roles can be assumed when idem is run with --test, but make sure that it exists first
    """
    # Verify that the role exists without "test"
    ret = run_yaml_block(NEW_ROLE_STATE, acct_data=acct_data)
    test_assume_role_ret = ret[
        "aws.iam.role_|-test_assume_role_|-test_assume_role_|-present"
    ]
    assert test_assume_role_ret["result"] is True, test_assume_role_ret["comment"]

    # Now run the state with --test and verify that the test role can be assumed
    state = f"{NEW_ROLE_STATE}\n{ASSUME_ROLE_STATE}\n{USE_NEW_PROFILE_STATE}"

    ret = run_yaml_block(state, acct_data=acct_data, test=True)

    test_assume_role_ret = ret[
        "aws.iam.role_|-test_assume_role_|-test_assume_role_|-present"
    ]
    assert test_assume_role_ret["result"] is True, test_assume_role_ret["comment"]

    assume_role_ret = ret["acct_|-assume_role_|-assume_role_|-profile"]
    assert assume_role_ret["result"] is True, assume_role_ret["comment"]

    test_use_new_profile_ret = ret[
        "aws.iam.role_|-test_use_new_profile_|-test_use_new_profile_|-present"
    ]
    assert test_use_new_profile_ret["result"] is True, test_use_new_profile_ret[
        "comment"
    ]
